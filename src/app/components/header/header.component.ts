import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/modules/auth/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isAuth: boolean = false;
  email: string = null;
  
  constructor(
    private readonly authService: AuthService
  ) { }

    ngOnInit(): void {
    this.isAuth = this.authService.isAuthenticated();
    this.email = this.authService.getEmail();
    }
  
  logOut(): void {
    return this.authService.logOut();
  }

}
