import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  form: FormGroup = new FormGroup({
    email: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required)
  })

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService
  ) { }

  submit(): void{
    if (this.form.invalid) {
      return;
    }
    const { email, password } = this.form.value;
    this.authService.login(email, password).pipe(
      tap(() => this.router.navigateByUrl('/admin/posts'))
    ).subscribe();
  }


  ngOnInit(): void {
  }

}
