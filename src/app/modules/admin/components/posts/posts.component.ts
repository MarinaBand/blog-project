import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from 'src/app/interfaces/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts: Post[] = [];

  displayedColumns: string[] = ['position', 'title', 'author', 'actions'];

  constructor(
    private route: ActivatedRoute,
    private readonly postServise: PostsService
  ) { }

  ngOnInit(): void {
    this.postServise.find().subscribe(
      (posts: Post[]) => this.posts = posts
    )
  }

  remove(id: string): void {
    this.posts = this.posts.filter(
        (post: Post) => post.id !== id
      );
    this.postServise.delete(id).subscribe();
  }

}
