import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { Post } from 'src/app/interfaces/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

post: Post;

  constructor(
    private route: ActivatedRoute,
    private readonly postsService: PostsService
  ) { }

  private loadPost(): void {
     this.route.params.pipe(
      switchMap((params: Params) => this.postsService.findOne(params.id).pipe(
        tap((post: Post) => this.post = post)
      ))
    ).subscribe();
  }
  
  ngOnInit(): void {
    this.loadPost()
  }
}
