import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Post } from 'src/app/interfaces/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss']
})
export class PostCreateComponent implements OnInit {

  constructor(
    private readonly router: Router,
    private readonly postsService: PostsService
  ) { }

    form: FormGroup = new FormGroup({
    title: new FormControl(null, Validators.required),
    author: new FormControl(null, Validators.required),
    content: new FormControl(null, Validators.required)
  })


  ngOnInit(): void {
  }

  submit(): void {
    const post: Post = this.form.value;
    this.postsService.create(post).subscribe(
      () => this.router.navigateByUrl('/admin/posts')
    );
  }

}
