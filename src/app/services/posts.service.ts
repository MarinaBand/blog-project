import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Post, PostsResponse } from '../interfaces/post';
import { map } from 'rxjs/operators';

@Injectable({
providedIn: 'root'
})
export class PostsService {

constructor(
private httpClient: HttpClient
) {}

/**
* Список всех постов
*/
find(): Observable<Post[]> {
return this.httpClient.get<PostsResponse>(`${environment.realtimeDBURL}/posts.json`).pipe(
map((res: PostsResponse) => {
const posts = [];

for (let id in res) {
posts.push({
...res[id],
id
});
}

return posts;
})
)
}

/**
* Получить по id
*/
findOne(id: string): Observable<Post> {
return this.httpClient.get<Post>(`${environment.realtimeDBURL}/posts/${id}.json`)
}

/**
* Создать
*/
create(post: Post): Observable <Post> {
    return this.httpClient.post<{ name: string }>(
        `${environment.realtimeDBURL}/posts.json`,
        post
    ).pipe(
        map((res: { name: string }) => ({ ...post, id: res.name }))
    );
}

/**
* Обновить по id
*/

update(id: string, post: Post): Observable <Post> {
return this.httpClient.put<Post>(`${environment.realtimeDBURL}/posts/${id}.json`, post)
}

/**
* Удалить по id
*/
delete(id: string): Observable <null> {
return this.httpClient.delete<null>(`${environment.realtimeDBURL}/posts/${id}.json`)
}
}